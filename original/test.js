var arrayObjetUsersItems = [];
var arraObjItem = [];
var user;
var dataSet = []

var tagsExpected = new Map();
tagsExpected.set('cart_eligible', cart_eligible = true);

var cont = 1;
var users = [
    272254074,
    395250614,
    395233059,
    403627009,
    395190573,
    395180839,
    394868484,
    394864202,
    394863571,
    394847088,
    393741033,
    393734694,
    392685356,
    392675081,
    392675005,
    391938304,
    391881950,
    390395020,
    390123542,
    390123496,
    390122820,
    390122762,
    390121286,
    390119156,
    390113830,
    390115694,
    390106567,
    389741095,
    388742551,
    388741325,
    388588226,
    379955493,
    377884955,
    377178384,
    375669986,
    372721887,
    372702920,
    370309766,
    366883651,
    362623808,
    362451205,
    362147748,
    362137386,
    361738655
];
var paises =
    {
        "MLA": {id: "MLA", nombre: "Argentina", dominio: "mercadolibre", com: ".com", pais: ".ar"},
        "MLB": {id: "MLB", nombre: "Brasil", dominio: "mercadolivre", com: ".com", pais: ".br"},
        "MCO": {id: "MCO", nombre: "Colombia", dominio: "mercadolibre", com: ".com", pais: ".co"},
        "MCR": {id: "MCR", nombre: "Costa Rica", dominio: "mercadolibre", com: ".co", pais: ".cr"},
        "MEC": {id: "MEC", nombre: "Ecuador", dominio: "mercadolibre", com: ".com", pais: ".ec"},
        "MLC": {id: "MLC", nombre: "Chile", dominio: "mercadolibre", com: ".cl", pais: ""},
        "MLM": {id: "MLM", nombre: "Mexico", dominio: "mercadolibre", com: ".com", pais: ".mx"},
        "MLU": {id: "MLU", nombre: "Uruguay", dominio: "mercadolibre", com: ".com", pais: ".uy"},
        "MLV": {id: "MLV", nombre: "Venezuela", dominio: "mercadolibre", com: ".com", pais: ".ve"},
        "MPA": {id: "MPA", nombre: "Panamá", dominio: "mercadolibre", com: ".com", pais: ".pa"},
        "MPE": {id: "MPE", nombre: "Perú", dominio: "mercadolibre", com: ".com", pais: ".pe"},
        "MPY": {id: "MPY", nombre: "Paraguay", dominio: "mercadolibre", com: ".com", pais: ".py"}
    }

for (var i = 0; i < users.length; i++) {

    user = users[i];
    var requestItems = new XMLHttpRequest();
    requestItems.open('GET', 'http://api.internal.ml.com/users/' + user + '/items/search?tags=test_item&caller.scopes=admin');
//primer request,
    requestItems.onreadystatechange = function () {
        if (this.readyState === 4 && this.status == '200') {
            items = JSON.parse(this.response).results;
            Object.keys(items).forEach(function (key) {
                arrayObjetUsersItems.push(items[key]);

                var item = items[key];
                var requestItem = new XMLHttpRequest();
                requestItem.open('GET', 'http://api.internal.ml.com/items/' + item + '?caller.scopes=admin');

                requestItem.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status == '200') {
                        jsonItem = JSON.parse(this.response);
                        if (jsonItem.status == 'active') {

                            var tags = jsonItem.tags;
                            var tagConcat = "";
                            tags.forEach(function (tag) {
                                // tagsExpected.get(tag);
                                tagConcat = tagConcat + " | " + tag;
                            });

                            var ob = {
                                site_id: jsonItem.site_id,
                                id: jsonItem.id,
                                seller_id: jsonItem.seller_id,
                                available_quantity: jsonItem.available_quantity,
                                local_pick_up: jsonItem.shipping.local_pick_up,
                                free_shipping: jsonItem.shipping.free_shipping,
                                store_pick_up: jsonItem.shipping.store_pick_up,
                                permalink: jsonItem.permalink

                            }

                            var siteActual = paises[ob.site_id]
                            var entornoDesa = "desa";
                            var entornoProd = "www";
                            var puerto = ":8080"
                            var dominio = siteActual.dominio;
                            var com = siteActual.com;
                            var extpais = siteActual.pais;
                            var pais = "";
                            var item = ob.id;
                            var cant = 1;

                            urlDesa = "http://" + entornoDesa + "." + dominio + com + extpais + "" + puerto + "/gz/checkout/direct/buy?item_id=" + pais + item + "&quantity=" + cant;
                            urlProd = "http://" + entornoProd + "." + dominio + com + extpais + "/gz/checkout/direct/buy?item_id=" + pais + item + "&quantity=" + cant;
                            itemUrl = "http://api.internal.ml.com/items/" + pais + item + "?caller.scopes=admin";
                            itemStore = "http://api.internal.ml.com/items/" + pais + item + "/stores?caller.scopes=admin";
                            linkAcceso = "<a title='DESA' href='" + urlDesa + "' target='_blank'> DESA </a> | <a title='PROD' href='" + urlProd + "' target='_blank'> PROD </a>";
                            datosItem = "<a title='Link de info del item' href='" + itemUrl + "' target='_blank' class='btn btn-xs" +
                                " btn-primary'><span class='glyphicon glyphicon-info-sign'></span></a>";
                            datosStore = "<a title='Stores asociados' href='" + itemStore + "' target='_blank' class='btn btn-xs" +
                                " btn-primary'><span class='glyphicon glyphicon-shopping-cart'></span></a>";
                            dataSet.push([cont, ob.site_id, ob.seller_id, ob.id, linkAcceso, ob.available_quantity, ob.local_pick_up, ob.free_shipping, ob.store_pick_up, tagConcat, datosItem + datosStore]);
                            cont++;
                        }
                    }

                };
                requestItem.send();
                setTimeout(function () {
                }, 1000);

            });
        }
    }
    requestItems.send();
    setTimeout(function () {
    }, 1000);

}

function cargarDataTable() {
    $('#example').DataTable({
        select: false,
        data: dataSet,
        columns: [
            {title: "N"},
            {title: "SITE"},
            {title: "SELLER_ID"},
            {title: "ITEM"},
            {title: "Acceso"},
            {title: "CANTIDAD"},
            {title: "local_pick_up"},
            {title: "free_shipping"},
            {title: "store_pick_up"},
            {title: "cart_eligible"},
            {title: "+Info"}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                // console.log(column)
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
}

setTimeout(function () {
    cargarDataTable();
}, 10000);