class Shipping {
    localPickUp;
    freeShipping;
    puis;

    constructor(shipping) {
        this.localPickUp = shipping.local_pick_up;
        this.freeShipping = shipping.free_shipping;
        this.puis = shipping.store_pick_up;
    }

}
// module.exports = Shipping;