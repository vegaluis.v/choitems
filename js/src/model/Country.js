class Country {
    country;

    constructor() {
        this.country =
            {
                "MLA": {id: "MLA", nombre: "Argentina", dominio: "mercadolibre", com: ".com", pais: ".ar"},
                "MLB": {id: "MLB", nombre: "Brasil", dominio: "mercadolivre", com: ".com", pais: ".br"},
                "MCO": {id: "MCO", nombre: "Colombia", dominio: "mercadolibre", com: ".com", pais: ".co"},
                "MCR": {id: "MCR", nombre: "Costa Rica", dominio: "mercadolibre", com: ".co", pais: ".cr"},
                "MEC": {id: "MEC", nombre: "Ecuador", dominio: "mercadolibre", com: ".com", pais: ".ec"},
                "MLC": {id: "MLC", nombre: "Chile", dominio: "mercadolibre", com: ".cl", pais: ""},
                "MLM": {id: "MLM", nombre: "Mexico", dominio: "mercadolibre", com: ".com", pais: ".mx"},
                "MLU": {id: "MLU", nombre: "Uruguay", dominio: "mercadolibre", com: ".com", pais: ".uy"},
                "MLV": {id: "MLV", nombre: "Venezuela", dominio: "mercadolibre", com: ".com", pais: ".ve"},
                "MPA": {id: "MPA", nombre: "Panamá", dominio: "mercadolibre", com: ".com", pais: ".pa"},
                "MPE": {id: "MPE", nombre: "Perú", dominio: "mercadolibre", com: ".com", pais: ".pe"},
                "MPY": {id: "MPY", nombre: "Paraguay", dominio: "mercadolibre", com: ".com", pais: ".py"}
            }
    }
}

// module.exports = Country;