// const EndPointRequest = require("./EndPointRequest");
// const Item = require("./Item");

class User {
    id;
    siteId;
    itemList = [];

    constructor(user) {
        this.id = user;
        this.obtainDataFromUser();
    }

    obtainDataFromUser() {
        let requestUser = new EndPointRequest();
        let itemList = requestUser.sendRequest('GET', 'http://api.internal.ml.com/users/' + this.id + '/items/search?tags=test_item&caller.scopes=admin');
        this.addItem(itemList);
        this.obtainSiteFromUser();
    }

    obtainSiteFromUser() {
        if (this.itemList.length >= 1) {
            this.siteId = this.itemList[0].substring(0, 2);
        }
    }

    getItemsFromThisUser(siteId) {
        if (this.siteId == siteId) {
            return this.itemList;
        }
        return this.itemList;

    }

    addItem(itemListid) {
        if (itemListid.isNotEmpty() && itemListid.results != null) {
            var itemList = itemListid.results;
            itemList.forEach(function (item) {
                this.itemList.push(new Item(item));
            })
        }
    }
}
// module.exports = User;