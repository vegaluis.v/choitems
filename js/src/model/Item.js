// const EndPointRequest = require("./EndPointRequest");
// const Shipping = require("./Shipping");

class Item {
    id;
    siteId;
    status;
    tags;
    sellerId;
    availableQuantity;
    shipping;
    permalink;

    constructor(id) {
        this.id = id;
        this.obtainDataFromItem();
    }


    obtainDataFromItem() {
        let requestItem = new EndPointRequest();
        let item = requestItem.sendRequest('GET', 'http://api.internal.ml.com/items/' + this.id + '?caller.scopes=admin');
        this.completeItem(item)
    }

    completeItem(requestItem) {
        if (requestItem.isNotEmpty()) {
            this.siteId = requestItem.site_id;
            this.status = requestItem.status;
            this.tags = requestItem.tags;
            this.sellerId = requestItem.seller_id;
            this.availableQuantity = requestItem.available_quantity;
            this.shipping = new Shipping(requestItem.shipping);
            this.permalink = requestItem.permalink;
        }
    }

}

// module.exports = Item;