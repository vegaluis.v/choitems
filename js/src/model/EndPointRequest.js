class EndPointRequest {

    constructor() {
        this.request = new XMLHttpRequest();
    }

    sendRequest(action, url) {

        this.request.open(action, url);

        this.request.onreadystatechange = function () {
            if (this.readyState === 4 && this.status == '200') {
                return JSON.parse(this.response);
            }
            return {};
        }
    }
}
// module.exports = EndPointRequest;