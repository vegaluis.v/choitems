class Country {
    country;

    constructor() {
        this.country =
            {
                "MLA": {id: "MLA", nombre: "Argentina", dominio: "mercadolibre", com: ".com", pais: ".ar"},
                "MLB": {id: "MLB", nombre: "Brasil", dominio: "mercadolivre", com: ".com", pais: ".br"},
                "MCO": {id: "MCO", nombre: "Colombia", dominio: "mercadolibre", com: ".com", pais: ".co"},
                "MCR": {id: "MCR", nombre: "Costa Rica", dominio: "mercadolibre", com: ".co", pais: ".cr"},
                "MEC": {id: "MEC", nombre: "Ecuador", dominio: "mercadolibre", com: ".com", pais: ".ec"},
                "MLC": {id: "MLC", nombre: "Chile", dominio: "mercadolibre", com: ".cl", pais: ""},
                "MLM": {id: "MLM", nombre: "Mexico", dominio: "mercadolibre", com: ".com", pais: ".mx"},
                "MLU": {id: "MLU", nombre: "Uruguay", dominio: "mercadolibre", com: ".com", pais: ".uy"},
                "MLV": {id: "MLV", nombre: "Venezuela", dominio: "mercadolibre", com: ".com", pais: ".ve"},
                "MPA": {id: "MPA", nombre: "Panamá", dominio: "mercadolibre", com: ".com", pais: ".pa"},
                "MPE": {id: "MPE", nombre: "Perú", dominio: "mercadolibre", com: ".com", pais: ".pe"},
                "MPY": {id: "MPY", nombre: "Paraguay", dominio: "mercadolibre", com: ".com", pais: ".py"}
            }
    }
}

class EndPointRequest {

    constructor() {
        this.request = new XMLHttpRequest();
    }

    sendRequest(action, url) {

        this.request.open(action, url);
        console.log(request);

        this.request.onreadystatechange = function () {

            if (this.readyState === 4 && this.status == '200') {
                console.log("entre");
                return JSON.parse(this.response);
            }
            return {respuesta:null};
        }
    }
}

class Item {
    id;
    siteId;
    status;
    tags;
    sellerId;
    availableQuantity;
    shipping;
    permalink;

    constructor(id) {
        this.id = id;
        this.obtainDataFromItem();
    }


    obtainDataFromItem() {
        let requestItem = new EndPointRequest();
        let item = requestItem.sendRequest('GET', 'http://api.internal.ml.com/items/' + this.id + '?caller.scopes=admin');
        this.completeItem(item)
    }

    completeItem(requestItem) {
        if (requestItem.length != 0) {
            this.siteId = requestItem.site_id;
            this.status = requestItem.status;
            this.tags = requestItem.tags;
            this.sellerId = requestItem.seller_id;
            this.availableQuantity = requestItem.available_quantity;
            this.shipping = new Shipping(requestItem.shipping);
            this.permalink = requestItem.permalink;
        }
    }

}

class Shipping {
    localPickUp;
    freeShipping;
    puis;

    constructor(shipping) {
        this.localPickUp = shipping.local_pick_up;
        this.freeShipping = shipping.free_shipping;
        this.puis = shipping.store_pick_up;
    }

}

class User {
    id;
    siteId;
    itemList = [];

    constructor(user) {
        this.id = user;
        this.obtainDataFromUser();
    }

    obtainDataFromUser() {
        let requestUser = new EndPointRequest();
        let itemList = requestUser.sendRequest('GET', 'http://api.internal.ml.com/users/' + this.id + '/items/search?tags=test_item&caller.scopes=admin');
        console.log(itemList);
        this.addItem(itemList);
        this.obtainSiteFromUser();
    }

    obtainSiteFromUser() {
        if (this.itemList.length >= 1) {
            this.siteId = this.itemList[0].substring(0, 2);
        }
    }

    getItemsFromThisUser(siteId) {
        if (this.siteId == siteId) {
            return this.itemList;
        }
        return this.itemList;

    }

    addItem(itemListid) {
        if (itemListid != null && itemListid.results != null) {
            var itemList = itemListid.results;
            itemList.forEach(function (item) {
                this.itemList.push(new Item(item));
            })
        }
    }
}

class UserGateway {
    arrayUserId = [];
    usersOnGateWay;


    constructor() {
        this.arrayUserId = this.conectToDb();
        this.resolverUsers();
    }

    conectToDb() {
        return new LocalDataBase().users;
    }

    resolverUsers() {
        let users = [];

        this.arrayUserId.forEach(function (user) {
            users.push(new User(user));
        })

        this.usersOnGateWay = users;
    }

    obtainUsers() {
        return this.usersOnGateWay;
    }

}

class LocalDataBase {
    users;

    constructor() {
        this.users = [
            362147748
        ];
    }
}

// =========================================================

const users = new UserGateway().obtainUsers();

console.log(users);